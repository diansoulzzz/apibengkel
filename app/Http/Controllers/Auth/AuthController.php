<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\MUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    public function redirect(Request $request)
    {
        return redirect('http://156.67.216.39:3000');
    }

    public function login(Request $request)
    {
        $input = $request->all();
        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];
        if (auth()->attempt($credentials)) {
            $user = MUser::find(auth()->id());
            /*
            $user->api_token = str_random(60);
            $user->save();
            */
            $data = $user->makeVisible('api_token');
            return response()->json($this->setSuccessResponse($data, $input));
        } else {
            return response()->json($this->setErrorResponse([], $input, 'Email atau password salah.'));
        }
    }

    public function unauthenticated(Request $request)
    {
        $input = $request->all();
        $error_messages = 'Unauthorized';
        return response()->json($this->setErrorResponse([], $input, $error_messages, [], 401));
    }

    public function register(Request $request)
    {
        $input = $request->all();
        $user = MUser::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'm_users_role_id' => '1',
            'password' => Hash::make($request->input('password')),
            'api_token' => str_random(60),
        ]);
        return response()->json($this->setSuccessResponse($user, $input));
    }

    public function checkStatus(Request $request)
    {
        $input = $request->all();
        $is_auth = false;
        $auth = $request->header('Authorization');
        $auth = str_replace('Bearer ', '', $auth);
        $user = MUser::where('api_token', $auth)->first();
        if ($user) {
            $user = $user->makeVisible('api_token');
            $is_auth = false;
        }
        $data = ['is_auth' => $is_auth, 'info' => $user];
        return response()->json($this->setSuccessResponse($data, $input));
    }

    public function checkLicense(Request $request)
    {
        $ip = request()->ip();
        $input = $request->all();
        return response()->json($this->setSuccessResponse([], $input));
        $paramName = strtoupper($request->input('nama_perusahaan'));
        $arrName = [
            'PT. BUILDYET INDONESIA',
        ];
        if (in_array($paramName, $arrName)) {
            return response()->json($this->setSuccessResponse([], $input));
        }
        return response()->json($this->setErrorResponse([], $input, 'Invalid License Key'));
    }
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    // use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('guest')->except('logout');
    // }
}
