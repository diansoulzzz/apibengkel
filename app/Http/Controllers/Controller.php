<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Carbon\Carbon;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function genFormatCode($header, $zero_count, $code)
    {
        return $header . sprintf('%0' . $zero_count . 'd', $code);
    }

    public function generateCode($code, Carbon $date, $counter)
    {
        $morph_counter = sprintf('%05d', $counter);
        return $code . '/' . $date->format('Y/md') . '/' . $morph_counter;
    }

    public function random_color_part()
    {
        return str_pad(dechex(mt_rand(0, 255)), 2, '0', STR_PAD_LEFT);
    }

    public function random_color()
    {
        return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
    }

    public function setSuccessResponse($data = [], $input = [], $message = 'OK', $options = [], $code = 200)
    {
        return [
            'data' => $data,
            'options' => $options,
            'input' => $input,
            'status' => [
                'code' => $code,
                'message' => $message,
            ],
        ];
    }

    public function setErrorResponse($error = [], $input = [], $message = 'Oops theres and error', $options = [], $code = 502)
    {
        return [
            'error' => $error,
            'options' => $options,
            'input' => $input,
            'status' => [
                'code' => $code,
                'message' => $message,
            ],
        ];
    }
}
