<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\MCustomer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CustomerController extends Controller
{
    public function data(Request $request)
    {
        $data = MCustomer::find($request->input('uid'));
        return response()->json($this->setSuccessResponse($data, $request->all()));
    }

    public function list(Request $request)
    {
        $columnDefs = collect([
            [
                'headerName' => 'ID',
                'field' => 'id',
                'hide' => true,
            ], [
                'headerName' => 'Code',
                'field' => 'code',
                'hide' => false,
            ], [
                'headerName' => 'Name',
                'field' => 'name',
                'hide' => false,
            ], [
                'headerName' => 'Create At',
                'field' => 'created_at',
                'hide' => false,
            ], [
                'headerName' => 'Update At',
                'field' => 'updated_at',
                'hide' => false,
            ],
        ]);
        $columns = $columnDefs->pluck('field')->all();
        $data = MCustomer::select($columns)->get();
        $defaultColDef = [
            'sortable' => true,
            'filter' => true,
            'resizable' => true,
        ];
        $ag_grid = [
            'defaultColDef' => $defaultColDef,
            'columnDefs' => $columnDefs,
            'rowData' => $data,
        ];
        $response = [
            'table' => $ag_grid,
        ];
        return response()->json($this->setSuccessResponse($response, $request->all()));
    }

    public function newCode(Request $request)
    {
        $data = MCustomer::select('code')->orderBy('code', 'desc')->first();
        $code = "1";
        if ($data) {
            $code = substr($data->code, 1) + 1;
        }
        $formated_code = $this->genFormatCode("C", "9", $code);
        $result = [
            'code' => $formated_code,
            'name' => ''
        ];
        return response()->json($this->setSuccessResponse($result, $request->all()));
    }

    public function entry(Request $request)
    {
        // return $request->all();s
        $validator = Validator::make($request->all(), [
            'code' => 'required|unique:m_customer,id,' . $request->input('id'),
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            $error_messages = "";
            foreach ($validator->errors()->all() as $error_message) {
                $error_messages .= $error_message . "\n";
            }
            return response()->json($this->setErrorResponse($validator->errors(), $request->all(), $error_messages));
        }
        $data = new MCustomer();
        if ($request->has('id')) {
            $data = MCustomer::find($request->input('id'));
        }
        $data->code = $request->input('code');
        $data->name = $request->input('name');
        $data->save();
        return response()->json($this->setSuccessResponse($data, $request->all(), "Pelanggan dengan kode " . $data->code . " berhasil disimpan"));
        // return $request->all();
    }
}
