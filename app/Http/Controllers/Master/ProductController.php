<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\MProduct;
use App\Models\MProductCategory;
use App\Models\MProductPriceBuy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

// use Illuminate\Database\Eloquent\Builder;
// use Illuminate\Database\Query\Builder;


class ProductController extends Controller
{
    public static function getColumns()
    {
        return [
            [
                'headerName' => 'ID',
                'field' => 'id',
                'fullField' => 'm_product.id',
                'hide' => true,
            ], [
                'headerName' => 'Kode',
                'field' => 'code',
                'fullField' => 'm_product.code',
                'hide' => false,
            ], [
                'headerName' => 'Nama',
                'field' => 'name',
                'fullField' => 'm_product.name',
                'hide' => false,
            ], [
                'headerName' => 'ID Kategori Produk',
                'field' => 'm_product_category_id',
                'fullField' => 'm_product.m_product_category_id',
                'hide' => false,
            ], [
                'headerName' => 'Nama Kategori Produk',
                'field' => 'm_product_category',
                'fullField' => 'm_product_category.name as m_product_category',
                'hide' => false,
            ], [
                'headerName' => 'Harga Beli',
                'field' => 'price_buy',
                'fullField' => DB::Raw('ifnull(m_product_price_buy.price,0) as price_buy'),
                'hide' => false,
            ], [
                'headerName' => 'Create At',
                'field' => 'created_at',
                'fullField' => 'm_product.created_at',
                'hide' => false,
            ], [
                'headerName' => 'Update At',
                'field' => 'updated_at',
                'fullField' => 'm_product.updated_at',
                'hide' => false,
            ],
        ];
    }

    public function withOptions()
    {
        $options['m_product_category_option'] = MProductCategory::get();
        return $options;
    }

    public function options(Request $request)
    {
        return response()->json($this->setSuccessResponse([], $request->all(), '', $this->withOptions()));
    }

    public function data(Request $request)
    {
        $data = MProduct::with(['m_product_category'])->find($request->input('uid'));

        if (!$data) {
            foreach ($this->getColumns() as $arrColumns) {
                $data[$arrColumns['field']] = '';
            }
        }
        return response()->json($this->setSuccessResponse($data, $request->all(), '', $this->withOptions()));
    }

    public function list(Request $request)
    {
        $query = '';
        if ($request->has('query')) {
            $query = $request->input('query');
        }
        $columnDefs = collect($this->getColumns());
        $columns = $columnDefs->pluck('fullField')->all();
        $data = DB::table('m_product')
            ->join('m_product_category', 'm_product.m_product_category_id', '=', 'm_product_category.id')
            ->leftJoin('m_product_price_buy', function ($q) {
                $q->on('m_product_price_buy.m_product_id', '=', 'm_product.id')
                    ->on(
                        'm_product_price_buy.id',
                        '=',
                        DB::raw('(select max(x.id) from m_product_price_buy x where x.m_product_id = m_product_price_buy.m_product_id)')
                    );
                //   ->where('photos.status', '=', 1);
            })
            // ->where('m_product.name', 'like', $query)
            ->whichLike(['m_product.code', 'm_product.name', 'm_product.m_product_category_id', 'm_product_category.code', 'm_product_category.name'], $query)
            ->select($columns)
            ->orderBy('id', 'asc')
            ->get();

        $defaultColDef = [
            'sortable' => true,
            'filter' => true,
            'resizable' => true,
        ];
        $ag_grid = [
            'defaultColDef' => $defaultColDef,
            'columnDefs' => $columnDefs,
            'rowData' => $data,
        ];
        $response = [
            'table' => $ag_grid,
        ];
        return response()->json($this->setSuccessResponse($response, $request->all()));
    }

    public function newCode(Request $request)
    {
        $data = MProduct::select('code')->orderBy('code', 'desc')->first();
        $code = "1";
        if ($data) {
            $code = substr($data->code, 1) + 1;
        }
        $formated_code = $this->genFormatCode("", "9", $code);
        // foreach (MProduct::getColumns() as $arrColumns) {
        //     $result[$arrColumns['field']] = '';
        // }
        // $result['code'] = $formated_code;
        // $result['price_buy'] = 0;
        $result = [
            'code' => $formated_code,
            'name' => '',
            'm_product_category_id' => '',
            'price_buy' => 0,
        ];
        return response()->json($this->setSuccessResponse($result, $request->all(), '', $this->withOptions()));
    }

    public function entry(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'code' => 'required|unique:m_product,id,' . $request->input('id'),
            'name' => 'required',
            'm_product_category_id' => 'required|exists:m_product_category,id',
            'price_buy' => 'required',
        ]);
        if ($validator->fails()) {
            $error_messages = "";
            foreach ($validator->errors()->all() as $error_message) {
                $error_messages .= $error_message . "\n";
            }
            return response()->json($this->setErrorResponse($validator->errors(), $request->all(), $error_messages, $this->withOptions()));
        }
        $data = new MProduct();
        $data->created_by = auth()->id();
        if ($request->has('id')) {
            $data = MProduct::find($request->input('id'));
        }
        $data->code = $request->input('code');
        $data->name = $request->input('name');
        $data->m_product_category_id = $request->input('m_product_category_id');
        $data->updated_by = auth()->id();
        $data->save();

        $price_buy = MProductPriceBuy::where('price', $request->input('price_buy'))->where('m_product_id', $data->id)->first();
        if (!$price_buy) {
            $price_buy = new MProductPriceBuy();
        }
        $price_buy->price = $request->input('price_buy');
        $price_buy->date_start = Carbon::now();
        $price_buy->date_end = Carbon::now();
        $price_buy->updated_by = auth()->id();
        $price_buy->created_by = auth()->id();
        $data->m_product_price_buys()->save($price_buy);

        return response()->json($this->setSuccessResponse($data, $request->all(), "Produk dengan kode " . $data->code . " berhasil disimpan"));
    }
}
