<?php

namespace App\Http\Controllers\Procurement;

use App\Http\Controllers\Controller;
use App\Models\MProduct;
use App\Models\MSupplier;
use App\Models\PeriodeProduct;
use App\Models\TPurchaseOrder;
use App\Models\TPurchaseOrderD;
use App\Models\TProcurement;
use App\Models\TProcurementD;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use PDF;

class PurchaseController extends Controller
{
    public function getColumns()
    {
        return [
            [
                'headerName' => 'ID',
                'field' => 'id',
                'fullField' => 't_procurement.id',
                'hide' => true,
            ], [
                'headerName' => 'Code',
                'field' => 'code',
                'fullField' => 't_procurement.code',
                'hide' => false,
            ], [
                'headerName' => 'Supplier',
                'field' => 'm_supplier_id',
                'fullField' => 't_procurement.m_supplier_id',
                'hide' => false,
            ], [
                'headerName' => 'Tanggal',
                'field' => 'trans_date',
                'fullField' => 't_procurement.trans_date',
                'hide' => false,
            ], [
                'headerName' => 'Notes',
                'field' => 'notes',
                'fullField' => 't_procurement.notes',
                'hide' => false,
            ], [
                'headerName' => 'Payment Method',
                'field' => 'payment_method_type',
                'fullField' => 't_procurement.payment_method_type',
                'hide' => false,
            ],
        ];
    }

    public function withOptions()
    {
        $options['m_supplier_option'] = MSupplier::get();
        $options['t_purchase_order_option'] = TPurchaseOrder::select(['id', 'code'])->get();
        $options['payment_method_type_option'] = [
            [
                'id' => 'CASH',
                'key' => 'CASH',
                'name' => 'CASH',
                'label' => 'CASH'
            ],
            [
                'id' => 'CREDIT',
                'key' => 'CREDIT',
                'name' => 'CREDIT',
                'label' => 'CREDIT'
            ]
        ];
        return $options;
    }

    public function options(Request $request)
    {
        return response()->json($this->setSuccessResponse([], $request->all(), '', $this->withOptions()));
    }

    public function data(Request $request)
    {
        $poid = $request->input('poid');
        $uid = $request->input('uid');
        $data = TProcurement::with(['t_procurement_ds', 'm_supplier'])->find($uid);
        if (!$data) {
            $data = $this->getNewData();
        } else {
            if ($data->t_purchase_order_id) {
                $poid = $data->t_purchase_order_id;
            }
        }
        if ($poid) {
            $po = TPurchaseOrder::with(['t_purchase_order_ds', 'm_supplier'])->find($poid);
            if ($po) {
                $data['t_purchase_order_id'] = $po->id;
                $data['t_purchase_order_code'] = $po->code;
                $data['notes'] = $po->notes;
                $data['m_supplier'] = $po->m_supplier;
                $data['m_supplier_id'] = $po->m_supplier_id;
                $data['grand_total'] = $po->grand_total;
                $data['t_procurement_ds'] = $po->t_purchase_order_ds;
            }
        }
        return response()->json($this->setSuccessResponse($data, $request->all(), '', $this->withOptions()));
    }

    public function getNewData()
    {
        foreach ($this->getColumns() as $arrColumns) {
            $data[$arrColumns['field']] = '';
        }
        $data['trans_date'] = Carbon::now()->toDateString();
        $data['grand_total'] = 0;
        $data['t_procurement_ds'] = [];
        return $data;
    }

    public function list(Request $request)
    {
        $columnDefs = collect([
            [
                'headerName' => 'ID',
                'field' => 'id',
                'hide' => true,
            ], [
                'headerName' => 'KodeNota',
                'field' => 'code',
                'hide' => false,
            ], [
                'headerName' => 'Nota PO',
                'field' => 't_purchase_order_id',
                'hide' => false,
            ], [
                'headerName' => 'Create At',
                'field' => 'created_at',
                'hide' => false,
            ], [
                'headerName' => 'Update At',
                'field' => 'updated_at',
                'hide' => false,
            ],
        ]);
        $columns = $columnDefs->pluck('field')->all();
        $data = TProcurement::select($columns)->get();
        $defaultColDef = [
            'sortable' => true,
            'filter' => true,
            'resizable' => true,
        ];
        $ag_grid = [
            'defaultColDef' => $defaultColDef,
            'columnDefs' => $columnDefs,
            'rowData' => $data,
        ];
        $response = [
            'table' => $ag_grid,
        ];
        return response()->json($this->setSuccessResponse($response, $request->all(), '', $this->withOptions()));
    }

    public function entry(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'trans_date' => 'required|date',
            'm_supplier_id' => 'required|exists:m_supplier,id',
            't_procurement_ds' => 'required',
            'payment_method_type' => 'required'
        ]);

        $barangs = [];
        $errors_list = [];
        $grand_total = 0;
        $details = $request->input('t_procurement_ds');
        $t_procurement_ds = [];
        foreach ($details as $detail) {
            $m_product = MProduct::find($detail['m_product_id']);

            $subtotal = $detail['price'] * $detail['qty'];
            $grand_total += $subtotal;
            $t_procurement_d = [
                'm_product_id' => $detail['m_product_id'],
                'qty' => $detail['qty'],
                'price' => $detail['price'],
                'subtotal' => $subtotal,
            ];
            $barangs[] = [
                'm_product_code' => $m_product->code,
                'm_product_id' => $m_product->id,
                'last_stock' => $m_product->last_stock,
                'old_qty' => 0,
                'new_qty' => $detail['qty'],
            ];
            if (isset($detail['t_purchase_order_d_id'])) {
                $t_procurement_d['t_purchase_order_d_id'] = $detail['t_purchase_order_d_id'];
            }
            $t_procurement_ds[] = $t_procurement_d;
        }
        $isEdit = TProcurement::with(['t_procurement_ds'])->find($request->input('id'));
        if ($isEdit) {
            foreach ($isEdit->t_procurement_ds as $old_detail) {
                if (!collect($barangs)->contains('m_product_id', $old_detail['m_product_id'])) {
                    $barangs[] = [
                        'm_product_code' => $old_detail->m_product->code,
                        'm_product_id' => $old_detail->m_product->id,
                        'last_stock' => $old_detail->m_product->last_stock,
                        'old_qty' => $old_detail->qty,
                        'new_qty' => 0,
                    ];
                }
            }

            foreach ($isEdit->t_procurement_ds as $old_detail) {
                foreach ($barangs as $key => $barang) {
                    if ($barang['m_product_id'] == $old_detail['m_product_id']) {
                        $barangs[$key]['old_qty'] = $old_detail['qty'];
                        continue;
                    }
                }
            }

            foreach ($barangs as $key => $barang) {
                $calc = ($barang['last_stock'] + $barang['new_qty']) - $barang['old_qty'];
                if ($calc < 0) {
                    $errors_list[] = "Stok Akan Menjadi Minus (" . $barang['m_product_code'] . "). Current Stok : (" . round($barang['last_stock'] - $barang['old_qty']) . "), Minus (" . $calc . ")";
                    continue;
                }
            }
        }

        //NOTA PO HANYA BISA DI REALISASI 1x
        if ($request->input('t_purchase_order_id') && $request->input('id') == '') {
            $t_purchase_order = TPurchaseOrder::find($request->input('t_purchase_order_id'));
            if ($t_purchase_order->is_closed) {
                $errors_list[] = "Nota PO sudah pernah direalisasi";
            }
        }

        if ($validator->fails() || (collect($errors_list)->isNotEmpty())) {
            $error_messages = "";
            foreach ($validator->errors()->all() as $error_message) {
                $error_messages .= $error_message . "\n";
            }
            foreach ($errors_list as $error_message) {
                $error_messages .= $error_message . "\n";
            }
            $error_messages = rtrim($error_messages, "\n");
            return response()->json($this->setErrorResponse($validator->errors(), $request->all(), $error_messages, $this->withOptions()));
        }
        $trans_date = Carbon::createFromDate($request->input('trans_date'));
        $last_code = TProcurement::where('trans_date', '=', $trans_date)->count() + 1;
        $codeNota = $this->generateCode('BO', $trans_date, $last_code);

        $data = new TProcurement();
        $data->created_by = auth()->id();
        $data->code = $codeNota;
        if ($request->has('id')) {
            if ($request->input('id') != '') {
                $data = TProcurement::find($request->input('id'));
            }
        }
        if ($request->input('t_purchase_order_id')) {
            $data->t_purchase_order_id = $request->input('t_purchase_order_id');
        }
        $data->trans_date = $trans_date;
        $data->m_supplier_id = $request->input('m_supplier_id');
        $data->notes = $request->input('notes');
        $data->grand_total = $grand_total;
        $data->updated_by = auth()->id();
        $data->payment_method_type = $request->input('payment_method_type');
        $data->save();
        $data->t_procurement_ds()->delete();

        $data->t_procurement_ds()->createMany($t_procurement_ds);
        if ($request->input('t_purchase_order_id')) {
            $data->t_purchase_order()->update([
                'is_closed' => 1,
            ]);
        }

//        UPDATE STOK
        foreach ($barangs as $key => $barang) {
            $u_barang = MProduct::find($barang['m_product_id']);
            $calc = ($u_barang->last_stock + $barang['new_qty']) - $barang['old_qty'];
            $u_barang->update(['last_stock' => $calc]);
        }

        $newData = $this->getNewData();
        $newData['before'] = $data;
        return response()->json($this->setSuccessResponse($newData, $request->all(), "BO dengan kodenota " . $data->code . " berhasil disimpan", $this->withOptions()));
    }

    public function rekalStok()
    {
        $periode_product = new PeriodeProduct();

    }

    public static function getProductColumns()
    {
        return [
            [
                'headerName' => 'ProductID',
                'field' => 'm_product_id',
                'fullField' => 'm_product.id as m_product_id',
                'hide' => true,
            ], [
                'headerName' => 'Kode',
                'field' => 'code',
                'fullField' => 'm_product.code',
                'hide' => false,
            ], [
                'headerName' => 'Nama',
                'field' => 'name',
                'fullField' => 'm_product.name',
                'hide' => false,
            ], [
                'headerName' => 'Nama Kategori Produk',
                'field' => 'm_product_category',
                'fullField' => 'm_product_category.name as m_product_category',
                'hide' => false,
            ], [
                'headerName' => 'Harga Beli',
                'field' => 'price_buy',
                'fullField' => DB::Raw('ifnull(m_product_price_buy.price,0) as price_buy'),
                'hide' => false,
            ],
        ];
    }

    public function searchProduct(Request $request)
    {
        $query = '';
        if ($request->has('query')) {
            $query = $request->input('query');
        }
        $columnDefs = collect($this->getProductColumns());
        $columns = $columnDefs->pluck('fullField')->all();
        $data = DB::table('m_product')
            ->join('m_product_category', 'm_product.m_product_category_id', '=', 'm_product_category.id')
            ->leftJoin('m_product_price_buy', function ($q) {
                $q->on('m_product_price_buy.m_product_id', '=', 'm_product.id')
                    ->on(
                        'm_product_price_buy.id',
                        '=',
                        DB::raw('(select max(x.id) from m_product_price_buy x where x.m_product_id = m_product_price_buy.m_product_id)')
                    );
                //   ->where('photos.status', '=', 1);
            })
            ->whichLike(['m_product.code', 'm_product.name', 'm_product.m_product_category_id', 'm_product_category.code', 'm_product_category.name'], $query)
            ->select($columns)
            ->orderBy('m_product_id', 'asc')
            ->get();

        $defaultColDef = [
            'sortable' => true,
            'filter' => true,
            'resizable' => true,
        ];
        $ag_grid = [
            'defaultColDef' => $defaultColDef,
            'columnDefs' => $columnDefs,
            'rowData' => $data,
        ];
        $response = [
            'table' => $ag_grid,
        ];
        return response()->json($this->setSuccessResponse($response, $request->all()));
    }

    public function printPurchase(Request $request)
    {
        $data = TProcurement::with(['m_supplier', 't_procurement_ds.m_product'])->find($request->input('uid'));
        $header = view('prints.purchase.header', compact('data'))->render();
        $footer = view('prints.purchase.footer', compact('data'))->render();
        $content = view('prints.purchase.content', compact('data'))->render();
        $pdf = PDF::setPaper('a5')
            ->setOrientation('landscape')
            ->setOption('margin-left', 4)
            ->setOption('margin-right', 4)
            ->setOption('margin-top', 30)
            ->setOption('margin-bottom', 40)
            ->setOption('header-html', $header)
            ->setOption('footer-html', $footer)
            ->loadHtml($content);
        // ->inline();
        $file = $pdf->download();
        $pdfJson = base64_encode($file);
        return response()->json($this->setSuccessResponse($pdfJson, $request->all()));
        // return response()->json([
        //     'pdf' => $pdfJson,
        // ]);
        // return $pdf->download();
        // return $pdf->inline();
    }
}
