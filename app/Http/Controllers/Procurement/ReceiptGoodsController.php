<?php

namespace App\Http\Controllers\Procurement;

use App\Http\Controllers\Controller;
use App\Models\MProduct;
use App\Models\MSupplier;
use App\Models\TPurchaseOrder;
use App\Models\TPurchaseOrderD;
use App\Models\TProcurement;
use App\Models\TProcurementD;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use PDF;

class ReceiptGoodsController extends Controller
{
    public function getColumns()
    {
        return [
            [
                'headerName' => 'ID',
                'field' => 'id',
                'fullField' => 't_procurement.id',
                'hide' => true,
            ], [
                'headerName' => 'Code',
                'field' => 'code',
                'fullField' => 't_procurement.code',
                'hide' => false,
            ], [
                'headerName' => 'Supplier',
                'field' => 'm_supplier_id',
                'fullField' => 't_procurement.m_supplier_id',
                'hide' => false,
            ], [
                'headerName' => 'Tanggal',
                'field' => 'trans_date',
                'fullField' => 't_procurement.trans_date',
                'hide' => false,
            ], [
                'headerName' => 'Notes',
                'field' => 'notes',
                'fullField' => 't_procurement.notes',
                'hide' => false,
            ],
        ];
    }

    public function withOptions()
    {
        $options['m_supplier_option'] = MSupplier::get();
        $options['t_purchase_order_option'] = TPurchaseOrder::select(['id', 'code'])->get();
        return $options;
    }

    public function options(Request $request)
    {
        return response()->json($this->setSuccessResponse([], $request->all(), '', $this->withOptions()));
    }

    public function data(Request $request)
    {
        $data = TProcurement::with(['t_procurement_ds', 'm_supplier'])->find($request->input('uid'));
        if (!$data) {
            // foreach ($this->getColumns() as $arrColumns) {
            //     $data[$arrColumns['field']] = '';
            // }
            // $data['trans_date'] = Carbon::now()->toDateString();
            // $data['t_purchase_order_d'] = [];
            $data = $this->getNewData();
        }
        return response()->json($this->setSuccessResponse($data, $request->all(), '', $this->withOptions()));
    }

    public function getNewData()
    {
        foreach ($this->getColumns() as $arrColumns) {
            $data[$arrColumns['field']] = '';
        }
        $data['trans_date'] = Carbon::now()->toDateString();
        $data['grand_total'] = 0;
        $data['t_procurement_ds'] = [];
        return $data;
    }

    public function list(Request $request)
    {
        $columnDefs = collect([
            [
                'headerName' => 'ID',
                'field' => 'id',
                'hide' => true,
            ], [
                'headerName' => 'KodeNota',
                'field' => 'code',
                'hide' => false,
            ], [
                'headerName' => 'Create At',
                'field' => 'created_at',
                'hide' => false,
            ], [
                'headerName' => 'Update At',
                'field' => 'updated_at',
                'hide' => false,
            ],
        ]);
        $columns = $columnDefs->pluck('field')->all();
        $data = TProcurement::select($columns)->get();
        $defaultColDef = [
            'sortable' => true,
            'filter' => true,
            'resizable' => true,
        ];
        $ag_grid = [
            'defaultColDef' => $defaultColDef,
            'columnDefs' => $columnDefs,
            'rowData' => $data,
        ];
        $response = [
            'table' => $ag_grid,
        ];
        return response()->json($this->setSuccessResponse($response, $request->all(), '', $this->withOptions()));
    }

    public function entry(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'trans_date' => 'required|date',
            'm_supplier_id' => 'required|exists:m_supplier,id',
            't_procurement_ds' => 'required'
            // 'grand_total' => 'required',
        ]);
        if ($validator->fails()) {
            $error_messages = "";
            foreach ($validator->errors()->all() as $error_message) {
                $error_messages .= $error_message . "\n";
            }
            return response()->json($this->setErrorResponse($validator->errors(), $request->all(), $error_messages, $this->withOptions()));
        }
        $trans_date = Carbon::createFromDate($request->input('trans_date'));
        $last_code = TProcurement::where('trans_date', '=', $trans_date)->count() + 1;
        $codeNota = $this->generateCode('BO', $trans_date, $last_code);
        $grand_total = 0;
        $details = $request->input('t_procurement_ds');
        // return $details;
        // return $details;
        // return $details[0]['id'];
        $t_procurement_ds = [];
        foreach ($details as $detail) {
            $subtotal = $detail['price'] * $detail['qty'];
            $grand_total += $subtotal;
            $t_procurement_d = [
                'm_product_id' => $detail['m_product_id'],
                'qty' => $detail['qty'],
                'price' => $detail['price'],
                'subtotal' => $subtotal,
            ];
            // $t_purchase_order_d = new TPurchaseOrderD([
            //     // 'id' => '',
            //     'm_product_id' => $detail['m_product_id'],
            //     'qty' => $detail['qty'],
            //     'price' => $detail['price'],
            //     'subtotal' => $subtotal,
            // ]);
            // if (isset($detail['id'])){
            //     $t_purchase_order_d['id'] = $detail['id'];
            // }
            $t_procurement_ds[] = $t_procurement_d;
        }
        $data = new TProcurement();
        $data->created_by = auth()->id();
        $data->code = $codeNota;
        if ($request->has('id')) {
            if ($request->input('id') != '') {
                $data = TProcurement::find($request->input('id'));
            }
        }
        $data->trans_date = $trans_date;
        $data->m_supplier_id = $request->input('m_supplier_id');
        $data->notes = $request->input('notes');
        $data->grand_total = $grand_total;
        $data->updated_by = auth()->id();
        $data->save();
        $data->t_purchase_order_ds()->delete();
        $data->t_purchase_order_ds()->createMany($t_procurement_ds);
        // foreach ($t_purchase_order_ds as $loop){
        //     $updateOrCreate = collect($loop)->pluck('id')->toArray();
        //     $data->t_purchase_order_ds()->updateOrCreate(['id' => $loop['id']], $updateOrCreate);
        // }
        // $data->t_purchase_order_ds()->saveMany($details);
        // $data = $this->getNewData();
        $newData = $this->getNewData();
        $newData['before'] = $data;
        return response()->json($this->setSuccessResponse($newData, $request->all(), "BO dengan kodenota " . $data->code . " berhasil disimpan", $this->withOptions()));
        // return $request->all();
    }

    public static function getProductColumns()
    {
        return [
            [
                'headerName' => 'ProductID',
                'field' => 'm_product_id',
                'fullField' => 'm_product.id as m_product_id',
                'hide' => true,
            ], [
                'headerName' => 'Kode',
                'field' => 'code',
                'fullField' => 'm_product.code',
                'hide' => false,
            ], [
                'headerName' => 'Nama',
                'field' => 'name',
                'fullField' => 'm_product.name',
                'hide' => false,
            ], [
                'headerName' => 'Nama Kategori Produk',
                'field' => 'm_product_category',
                'fullField' => 'm_product_category.name as m_product_category',
                'hide' => false,
            ], [
                'headerName' => 'Harga Beli',
                'field' => 'price_buy',
                'fullField' => DB::Raw('ifnull(m_product_price_buy.price,0) as price_buy'),
                'hide' => false,
            ],
        ];
    }

    public function searchProduct(Request $request)
    {
        $query = '';
        if ($request->has('query')) {
            $query = $request->input('query');
        }
        $columnDefs = collect($this->getProductColumns());
        $columns = $columnDefs->pluck('fullField')->all();
        $data = DB::table('m_product')
            ->join('m_product_category', 'm_product.m_product_category_id', '=', 'm_product_category.id')
            ->leftJoin('m_product_price_buy', function ($q) {
                $q->on('m_product_price_buy.m_product_id', '=', 'm_product.id')
                    ->on(
                        'm_product_price_buy.id',
                        '=',
                        DB::raw('(select max(x.id) from m_product_price_buy x where x.m_product_id = m_product_price_buy.m_product_id)')
                    );
                //   ->where('photos.status', '=', 1);
            })
            ->whichLike(['m_product.code', 'm_product.name', 'm_product.m_product_category_id', 'm_product_category.code', 'm_product_category.name'], $query)
            ->select($columns)
            ->orderBy('m_product_id', 'asc')
            ->get();

        $defaultColDef = [
            'sortable' => true,
            'filter' => true,
            'resizable' => true,
        ];
        $ag_grid = [
            'defaultColDef' => $defaultColDef,
            'columnDefs' => $columnDefs,
            'rowData' => $data,
        ];
        $response = [
            'table' => $ag_grid,
        ];
        return response()->json($this->setSuccessResponse($response, $request->all()));
    }

    public function printPurchaseOrder(Request $request)
    {
        $data = TPurchaseOrder::with(['m_supplier', 't_purchase_order_ds.m_product'])->find($request->input('uid'));
        $header = view('prints.purchase-order.header', compact('data'))->render();
        $footer = view('prints.purchase-order.footer', compact('data'))->render();
        $content = view('prints.purchase-order.content', compact('data'))->render();
        $pdf = PDF::setPaper('a5')
            ->setOrientation('landscape')
            ->setOption('margin-left', 4)
            ->setOption('margin-right', 4)
            // ->setOption('margin-top', 30)
            // ->setOption('margin-bottom', 40)
            ->setOption('header-html', $header)
            ->setOption('footer-html', $footer)
            ->loadHtml($content);
        // ->inline();
        $file = $pdf->download();
        $pdfJson = base64_encode($file);
        return response()->json($this->setSuccessResponse($pdfJson, $request->all()));
        // return response()->json([
        //     'pdf' => $pdfJson,
        // ]);
        // return $pdf->download();
        // return $pdf->inline();
    }
}
