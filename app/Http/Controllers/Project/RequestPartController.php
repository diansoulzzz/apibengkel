<?php

namespace App\Http\Controllers\Project;

use App\Http\Controllers\Controller;
use App\Models\MProduct;
use App\Models\MSupplier;
use App\Models\TPurchaseOrder;
use App\Models\TPurchaseOrderD;
use App\Models\TFormPermintaanBarang;
use App\Models\TFormPermintaanBarangD;
use App\Models\TWorkOrder;
use App\Models\MCustomer;
use App\Models\MMekanik;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use PDF;

class RequestPartController extends Controller
{
    public function getColumns()
    {
        return [
            [
                'headerName' => 'ID',
                'field' => 'id',
                'fullField' => 't_form_permintaan_barang.id',
                'hide' => true,
            ], [
                'headerName' => 'Code',
                'field' => 'code',
                'fullField' => 't_form_permintaan_barang.code',
                'hide' => false,
            ], [
                'headerName' => 'Supplier',
                'field' => 'm_supplier_id',
                'fullField' => 't_form_permintaan_barang.m_supplier_id',
                'hide' => false,
            ], [
                'headerName' => 'Tanggal',
                'field' => 'trans_date',
                'fullField' => 't_form_permintaan_barang.trans_date',
                'hide' => false,
            ],
            // [
            //     'headerName' => 'Keterangan',
            //     'field' => 'notes',
            //     'fullField' => 't_form_permintaan_barang.notes',
            //     'hide' => false,
            // ],
        ];
    }

    public function withOptions()
    {
        $options['t_work_order_option'] = TWorkOrder::get();
        return $options;
    }

    public function options(Request $request)
    {
        return response()->json($this->setSuccessResponse([], $request->all(), '', $this->withOptions()));
    }

    public function data(Request $request)
    {
        $data = TFormPermintaanBarang::with(['t_work_order', 't_form_permintaan_barang_ds.m_product'])->where('t_work_order_id', $request->input('woid'))->first();
        if (!$data) {
            $data = $this->getNewData();
        }
        $wo = TWorkOrder::find($request->input('woid'));
        $data['t_work_order_id'] = $wo->id;
        $data['t_work_order_code'] = $wo->code;
        return response()->json($this->setSuccessResponse($data, $request->all(), '', $this->withOptions()));
    }

    public function getNewData()
    {
        foreach ($this->getColumns() as $arrColumns) {
            $data[$arrColumns['field']] = '';
        }
        $data['trans_date'] = Carbon::now()->toDateString();
        $data['grand_total'] = 0;
        $data['t_form_permintaan_barang_ds'] = [];
        return $data;
    }

    public function list(Request $request)
    {
        $columnDefs = collect([
            [
                'headerName' => 'ID',
                'field' => 'id',
                'hide' => true,
            ], [
                'headerName' => 'KodeNota',
                'field' => 'code',
                'hide' => false,
            ], [
                'headerName' => 'Create At',
                'field' => 'created_at',
                'hide' => false,
            ], [
                'headerName' => 'Update At',
                'field' => 'updated_at',
                'hide' => false,
            ],
        ]);
        $columns = $columnDefs->pluck('field')->all();
        $data = TFormPermintaanBarang::select($columns)->get();
        $defaultColDef = [
            'sortable' => true,
            'filter' => true,
            'resizable' => true,
        ];
        $ag_grid = [
            'defaultColDef' => $defaultColDef,
            'columnDefs' => $columnDefs,
            'rowData' => $data,
        ];
        $response = [
            'table' => $ag_grid,
        ];
        return response()->json($this->setSuccessResponse($response, $request->all(), '', $this->withOptions()));
    }

    public function entry(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'trans_date' => 'required|date',
            't_work_order_id' => 'required|exists:t_work_order,id',
            't_form_permintaan_barang_ds' => 'required'
        ]);
        $errors_list = [];
        $barangs = [];
        $details = $request->input('t_form_permintaan_barang_ds');
        $t_form_permintaan_barang_ds = [];
        foreach ($details as $detail) {
            $m_product = MProduct::find($detail['m_product_id']);
            $t_form_permintaan_barang_d = [
                'm_product_id' => $detail['m_product_id'],
                'qty' => $detail['qty'],
            ];
            $barangs[] = [
                'm_product_code' => $m_product->code,
                'm_product_id' => $m_product->id,
                'last_stock' => $m_product->last_stock,
                'old_qty' => 0,
                'new_qty' => $detail['qty'],
            ];
            $t_form_permintaan_barang_ds[] = $t_form_permintaan_barang_d;
        }

        $isEdit = TFormPermintaanBarang::with(['t_form_permintaan_barang_ds'])->find($request->input('id'));
        if ($isEdit) {
            foreach ($isEdit->t_form_permintaan_barang_ds as $old_detail) {
                if (!collect($barangs)->contains('m_product_id', $old_detail['m_product_id'])) {
                    $barangs[] = [
                        'm_product_code' => $old_detail->m_product->code,
                        'm_product_id' => $old_detail->m_product->id,
                        'last_stock' => $old_detail->m_product->last_stock,
                        'old_qty' => $old_detail->qty,
                        'new_qty' => 0,
                    ];
                }
            }

            foreach ($isEdit->t_form_permintaan_barang_ds as $old_detail) {
                foreach ($barangs as $key => $barang) {
                    if ($barang['m_product_id'] == $old_detail['m_product_id']) {
                        $barangs[$key]['old_qty'] = $old_detail['qty'];
                        continue;
                    }
                }
            }

            foreach ($barangs as $key => $barang) {
                $calc = ($barang['last_stock'] - $barang['new_qty']) + $barang['old_qty'];
                if ($calc < 0) {
                    $errors_list[] = "Ada Stok Tidak Mencukupi (" . $barang['m_product_code'] . "). Current Stok : (" . round($barang['last_stock'] + $barang['old_qty']) . "), Minus (" . $calc . ")";
                    continue;
                }
            }
        }

        if ($validator->fails() || (collect($errors_list)->isNotEmpty())) {
            $error_messages = "";
            foreach ($validator->errors()->all() as $error_message) {
                $error_messages .= $error_message . "\n";
            }
            foreach ($errors_list as $error_message) {
                $error_messages .= $error_message . "\n";
            }
            return response()->json($this->setErrorResponse($validator->errors(), $request->all(), $error_messages, $this->withOptions()));
        }
        $trans_date = Carbon::createFromDate($request->input('trans_date'));
        $last_code = TFormPermintaanBarang::where('trans_date', '=', $trans_date)->count() + 1;
        $codeNota = $this->generateCode('RP', $trans_date, $last_code);

        $data = new TFormPermintaanBarang();
        $data->created_by = auth()->id();
        $data->code = $codeNota;
        if ($request->has('id')) {
            if ($request->input('id') != '') {
                $data = TFormPermintaanBarang::find($request->input('id'));
            } else {
//                TWorkOrder::find($request->input('t_work_order_id'))->update(['status' => 1]);
            }
        }
        $data->trans_date = $trans_date;
        $data->t_work_order_id = $request->input('t_work_order_id');
        $data->updated_by = auth()->id();
        $data->save();
        $data->t_form_permintaan_barang_ds()->delete();
        $data->t_form_permintaan_barang_ds()->createMany($t_form_permintaan_barang_ds);

//        UPDATE STOK
        foreach ($barangs as $key => $barang) {
            $u_barang = MProduct::find($barang['m_product_id']);
            $calc = ($u_barang->last_stock - $barang['new_qty']) + $barang['old_qty'];
            $u_barang->update(['last_stock' => $calc]);
        }

        $newData = $this->getNewData();
        $newData['before'] = $data;
        return response()->json($this->setSuccessResponse($newData, $request->all(), "RP dengan kodenota " . $data->code . " berhasil disimpan", $this->withOptions()));
    }

    public static function getProductColumns()
    {
        return [
            [
                'headerName' => 'ProductID',
                'field' => 'm_product_id',
                'fullField' => 'm_product.id as m_product_id',
                'hide' => true,
            ], [
                'headerName' => 'Kode',
                'field' => 'code',
                'fullField' => 'm_product.code',
                'hide' => false,
            ], [
                'headerName' => 'Nama',
                'field' => 'name',
                'fullField' => 'm_product.name',
                'hide' => false,
            ], [
                'headerName' => 'Nama Kategori Produk',
                'field' => 'm_product_category',
                'fullField' => 'm_product_category.name as m_product_category',
                'hide' => false,
            ],
            // [
            //     'headerName' => 'Harga Beli',
            //     'field' => 'price_buy',
            //     'fullField' => DB::Raw('ifnull(m_product_price_buy.price,0) as price_buy'),
            //     'hide' => true,
            // ],
        ];
    }

    public function searchProduct(Request $request)
    {
        $query = '';
        if ($request->has('query')) {
            $query = $request->input('query');
        }
        $columnDefs = collect($this->getProductColumns());
        $columns = $columnDefs->pluck('fullField')->all();
        $data = DB::table('m_product')
            ->join('m_product_category', 'm_product.m_product_category_id', '=', 'm_product_category.id')
            // ->leftJoin('m_product_price_buy', function ($q) {
            //     $q->on('m_product_price_buy.m_product_id', '=', 'm_product.id')
            //         ->on(
            //             'm_product_price_buy.id',
            //             '=',
            //             DB::raw('(select max(x.id) from m_product_price_buy x where x.m_product_id = m_product_price_buy.m_product_id)')
            //         );
            // })
            ->whichLike(['m_product.code', 'm_product.name', 'm_product.m_product_category_id', 'm_product_category.code', 'm_product_category.name'], $query)
            ->select($columns)
            ->orderBy('m_product_id', 'asc')
            ->get();

        $defaultColDef = [
            'sortable' => true,
            'filter' => true,
            'resizable' => true,
        ];
        $ag_grid = [
            'defaultColDef' => $defaultColDef,
            'columnDefs' => $columnDefs,
            'rowData' => $data,
        ];
        $response = [
            'table' => $ag_grid,
        ];
        return response()->json($this->setSuccessResponse($response, $request->all()));
    }

    public function printRequestPart(Request $request)
    {
        $data = TFormPermintaanBarang::with(['t_work_order', 't_form_permintaan_barang_ds.m_product'])->find($request->input('uid'));
        $header = view('prints.request-part.header', compact('data'))->render();
        $footer = view('prints.request-part.footer', compact('data'))->render();
        $content = view('prints.request-part.content', compact('data'))->render();
        $pdf = PDF::setPaper('a5')
            ->setOrientation('landscape')
            ->setOption('margin-left', 4)
            ->setOption('margin-right', 4)
            ->setOption('margin-top', 30)
            ->setOption('margin-bottom', 40)
            ->setOption('header-html', $header)
            ->setOption('footer-html', $footer)
            ->loadHtml($content);
        // ->inline();
        $file = $pdf->download();
        $pdfJson = base64_encode($file);
        return response()->json($this->setSuccessResponse($pdfJson, $request->all()));
    }
}
