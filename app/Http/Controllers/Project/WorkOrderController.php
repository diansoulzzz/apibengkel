<?php

namespace App\Http\Controllers\Project;

use App\Http\Controllers\Controller;
use App\Models\MProduct;
use App\Models\MSupplier;
use App\Models\TPurchaseOrder;
use App\Models\TPurchaseOrderD;
use App\Models\TWorkOrder;
use App\Models\MCustomer;
use App\Models\MMekanik;
use App\Models\TWorkOrderPhoto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use PDF;
use Illuminate\Support\Facades\Storage;

class WorkOrderController extends Controller
{
    public function getColumns()
    {
        return [
            [
                'headerName' => 'ID',
                'field' => 'id',
                'fullField' => 't_work_order.id',
                'hide' => true,
            ], [
                'headerName' => 'Code',
                'field' => 'code',
                'fullField' => 't_work_order.code',
                'hide' => false,
            ], [
                'headerName' => 'Supplier',
                'field' => 'm_supplier_id',
                'fullField' => 't_work_order.m_supplier_id',
                'hide' => false,
            ], [
                'headerName' => 'Tanggal',
                'field' => 'trans_date',
                'fullField' => 't_work_order.trans_date',
                'hide' => false,
            ], [
                'headerName' => 'Komplain',
                'field' => 'complaint',
                'fullField' => 't_work_order.complaint',
                'hide' => false,
            ],
        ];
    }

    public function withOptions()
    {
        $options['m_customer_option'] = MCustomer::get();
        $options['m_mekanik_option'] = MMekanik::get();
        return $options;
    }

    public function options(Request $request)
    {
        return response()->json($this->setSuccessResponse([], $request->all(), '', $this->withOptions()));
    }

    public function data(Request $request)
    {
        $data = TWorkOrder::with(['m_customer', 'm_mekanik'])->find($request->input('uid'));
        if (!$data) {
            $data = $this->getNewData();
        }
        return response()->json($this->setSuccessResponse($data, $request->all(), '', $this->withOptions()));
    }

    public function getNewData()
    {
        foreach ($this->getColumns() as $arrColumns) {
            $data[$arrColumns['field']] = '';
        }
        $data['trans_date'] = Carbon::now()->toDateString();
        return $data;
    }

    public function list(Request $request)
    {
        $columnDefs = collect([
            [
                'headerName' => 'ID',
                'field' => 'id',
                'fullField' => 'wo.id',
                'hide' => true,
            ], [
                'headerName' => 'KodeNota',
                'field' => 'code',
                'fullField' => 'wo.code',
                'hide' => false,
            ], [
                'headerName' => 'Nomor Lambung',
                'field' => 'hull_number',
                'fullField' => 'wo.hull_number',
                'hide' => false,
            ], [
                'headerName' => 'Plat Nomor',
                'field' => 'plate_number',
                'fullField' => 'wo.plate_number',
                'hide' => false,
            ], [
                'headerName' => 'Komplain',
                'field' => 'complaint',
                'fullField' => 'wo.complaint',
                'hide' => false,
            ], [
                'headerName' => 'Status',
                'field' => 'status',
                'fullField' => 'wo.status',
                'hide' => true,
            ], [
                'headerName' => 'Progress',
                'field' => 'progress',
                'fullField' => DB::Raw(
                    'case wo.status
                            when 0 then "BARU"
                            when 1 then "SEDANG DIPERBAIKI"
                            when 2 then "SELESAI"
                            end as progress'
                ),
                'hide' => false,
            ], [
                'headerName' => 'With Part',
                'field' => 'is_with_part',
                'fullField' => DB::Raw(
                    'case when ifnull(pb.id,0)=0 then 0 ELSE 1 end as is_with_part'
                ),
                'hide' => true,
            ], [
                'headerName' => 'Dengan Part',
                'field' => 'with_part',
                'fullField' => DB::Raw(
                    'case when ifnull(pb.id,0)=0 then "TIDAK" ELSE "YA" end as with_part'
                ),
                'hide' => false,
            ], [
                'headerName' => 'Create At',
                'field' => 'created_at',
                'fullField' => 'wo.created_at',
                'hide' => false,
            ], [
                'headerName' => 'Update At',
                'field' => 'updated_at',
                'fullField' => 'wo.updated_at',
                'hide' => false,
            ],
        ]);

        $columns = $columnDefs->pluck('fullField')->all();
        $data = DB::table('t_work_order as wo')
            ->leftJoin('t_form_permintaan_barang as pb', 'wo.id', '=', 'pb.t_work_order_id')
            ->select($columns)
            ->orderBy('wo.trans_date', 'desc')
            ->get();

        $defaultColDef = [
            'sortable' => true,
            'filter' => true,
            'resizable' => true,
        ];
        $ag_grid = [
            'defaultColDef' => $defaultColDef,
            'columnDefs' => $columnDefs,
            'rowData' => $data,
        ];
        $response = [
            'table' => $ag_grid,
        ];
        return response()->json($this->setSuccessResponse($response, $request->all(), '', $this->withOptions()));
    }

    public function entry(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'trans_date' => 'required|date',
            'm_customer_id' => 'required|exists:m_customer,id',
            'm_mekanik_id' => 'required|exists:m_mekanik,id',
            'complaint' => 'required'
        ]);
        if ($validator->fails()) {
            $error_messages = "";
            foreach ($validator->errors()->all() as $error_message) {
                $error_messages .= $error_message . "\n";
            }
            return response()->json($this->setErrorResponse($validator->errors(), $request->all(), $error_messages, $this->withOptions()));
        }
        $trans_date = Carbon::createFromDate($request->input('trans_date'));
        $last_code = TWorkOrder::where('trans_date', '=', $trans_date)->count() + 1;
        $codeNota = $this->generateCode('WO', $trans_date, $last_code);
        $data = new TWorkOrder();
        $data->created_by = auth()->id();
        $data->code = $codeNota;
        if ($request->has('id')) {
            if ($request->input('id') != '') {
                $data = TWorkOrder::find($request->input('id'));
            } else {
                $data->status = 0;
            }
        }
        $data->trans_date = $trans_date;
        $data->m_customer_id = $request->input('m_customer_id');
        $data->m_mekanik_id = $request->input('m_mekanik_id');
        $data->complaint = $request->input('complaint');
        $data->plate_number = $request->input('plate_number');
        $data->hull_number = $request->input('hull_number');
        $data->updated_by = auth()->id();
        $data->save();

        $newData = $this->getNewData();
        $newData['before'] = $data;
        return response()->json($this->setSuccessResponse($newData, $request->all(), "WO dengan kodenota " . $data->code . " berhasil disimpan", $this->withOptions()));
    }

    public function setStatus(Request $request)
    {
        $data = TWorkOrder::with(['m_customer', 'm_mekanik'])->find($request->input('uid'));
        $data->status = $request->input('status');
        $data->save();
        return $this->list($request);
    }

    public function printWorkOrder(Request $request)
    {
        $data = TWorkOrder::with(['m_customer', 'm_mekanik'])->find($request->input('uid'));
        $header = view('prints.work-order.header', compact('data'))->render();
        $footer = view('prints.work-order.footer', compact('data'))->render();
        $content = view('prints.work-order.content', compact('data'))->render();
        $pdf = PDF::setPaper('a5')
            ->setOrientation('landscape')
            ->setOption('margin-left', 4)
            ->setOption('margin-right', 4)
            ->setOption('margin-top', 30)
            ->setOption('margin-bottom', 40)
            ->setOption('header-html', $header)
            ->setOption('footer-html', $footer)
            ->loadHtml($content);
        // ->inline();
        $file = $pdf->download();
        $pdfJson = base64_encode($file);
        return response()->json($this->setSuccessResponse($pdfJson, $request->all()));
    }

    public function dataUploadImage(Request $request)
    {
        $data = $this->getDataImage($request);
        return response()->json($this->setSuccessResponse($data, $request->all(), '', $this->withOptions()));
    }

    public function getDataImage(Request $request)
    {
        $data = TWorkOrder::with(['m_customer', 'm_mekanik', 't_work_order_photos'])->find($request->input('woid'));
        if (!$data) {
            $data = $this->getNewData();
        }
        return $data;
    }

    public function uploadImage(Request $request, $woid)
    {
        if ($request->hasFile('file')) {
            $fileContents = $request->file('file');
            //            $url = Storage::disk('public')->move($old, str_replace('temp/', '', $old));
            return Storage::disk('public')->put('temp/work-order/' . $woid, $fileContents);
        }
    }

    public function entryUploadImage(Request $request)
    {
        $data = TWorkOrder::find($request->input('woid'));
//        return $request->all();
        $fileUrls = $request->input('fileUrls');
        $t_work_order_photos = [];
        foreach ($fileUrls as $fileUrl) {
            if (Storage::disk('public')->exists($fileUrl)) {
                $newFileUrl = str_replace('temp/', '', $fileUrl);
                if (Storage::disk('public')->move($fileUrl, $newFileUrl)) {
                    $t_work_order_photo = [
                        'photo_url' => $newFileUrl,
                        'updated_by' => auth()->id(),
                        'created_by' => auth()->id(),
                    ];
                    $t_work_order_photos[] = $t_work_order_photo;
                }
            }
        }
        if ($t_work_order_photos) {
            $data->t_work_order_photos()->createMany($t_work_order_photos);
        }
        $data = $this->getDataImage($request);
        return response()->json($this->setSuccessResponse($data, $request->all(), 'Unggah Bukti Part Sukses', $this->withOptions()));
    }
}
