<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use App\Models\MReport;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ReportController extends Controller
{
    protected $delimiter = "_";
    protected $needle = "~";

    function strpos_all($haystack, $needle, $split_by)
    {
        $offset = 0;
        $allPos = [];
        $arrPos = [];
        $loop = 0;
        while (($pos = strpos($haystack, $needle, $offset)) !== FALSE) {
            $offset = $pos + 1;
            $arrPos[] = $pos;
            if ($split_by > 0) {
                if ($loop % $split_by) {
                    $allPos[] = $arrPos;
                    $arrPos = [];
                }
            } else {
                $allPos[] = $pos;
            }
            $loop++;
        }
        return $allPos;
    }

    public function getParam($queryString)
    {
        $allPos = $this->strpos_all($queryString, $this->needle, 2);
        $strPos = [];
        foreach ($allPos as $i => $val) {
            $name = substr($queryString, $val[0], $val[1] - $val[0] + 1);
            $type = 'STRING';
            $value = '';
            $split = explode($this->delimiter, substr($queryString, ($val[0] + 1), $val[1] - $val[0] + 1));
            $label = $split[0];
            if (strpos(strtoupper($name), 'STRING')) {
                $type = 'STRING';
                $value = '';
            }
            if (strpos(strtoupper($name), 'DATETIME')) {
                $type = 'DATETIME';
                $value = Carbon::now();
            }
            if (strpos(strtoupper($name), 'CHOICE')) {
                $type = 'CHOICE';
                $choicePos = $this->strpos_all($name, '^', 0);
                $choiceQuery = substr($name, ($choicePos[0] + 1), $choicePos[1] - ($choicePos[0] + 1));
                $value = DB::select($choiceQuery);
            }
            $strPos[] = ['key' => $i, 'name' => $name, 'label' => $label, 'type' => $type, 'value' => $value];
        }
        return $strPos;
    }

    public function getResult($queryString, $requestParams)
    {
        $queryParam = $this->getParam($queryString);
        foreach ($requestParams as $i => $val) {
            $i = strtoupper($i);
            foreach ($queryParam as $item => $value) {
                if ($value['label'] == $i) {
                    $queryString = str_replace($value['name'], $val, $queryString);
                }
            }
        }
        return $queryString;
    }

    public function generateParam(Request $request, $code, $title, $query)
    {
        $params = $this->getParam($query);
        $form = [];
        foreach ($params as $i => $val) {
            $form[$val['label']] = '';
        }
        $response = [
            'code' => $code,
            'title' => $title,
            'params' => $params,
            'form' => $form,
        ];
        return response()->json($this->setSuccessResponse($response, $request->all()));
    }

    public function generateResult(Request $request, $code, $title, $query)
    {
        $params = $request->except(['uid']);
        $queryResult = $this->getResult($query, $params);
        Log::info('ReportController@generateResult:' . $queryResult);
        $data = collect(DB::select($queryResult));

        $collect = collect($data->first());
        $columnDefs = [];
        foreach ($collect->keys() as $i => $val) {
            $columnDefs[] = ['field' => $val];
        }
        $defaultColDef = [
            'sortable' => true,
            'filter' => true,
            'resizable' => true,
        ];
        $ag_grid = [
            'defaultColDef' => $defaultColDef,
            'columnDefs' => $columnDefs,
            'rowData' => $data,
        ];
        $response = [
            'code' => $code,
            'title' => $title,
            'table' => $ag_grid,
        ];
        return response()->json($this->setSuccessResponse($response, $request->all()));
    }

    public function getParamFromID(Request $request)
    {
        $id = $request->input('uid');
        $report = MReport::find($id);
        return $this->generateParam($request, $report->code, $report->name, $report->query);
    }

    public function getResultFromID(Request $request)
    {
        $id = $request->input('uid');
        $report = MReport::find($id);
        return $this->generateResult($request, $report->code, $report->name, $report->query);
    }

}
