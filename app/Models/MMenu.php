<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 01 Sep 2019 14:43:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class MMenu
 * 
 * @property int $id
 * @property string $name
 * @property string $url
 * @property int $m_menus_id
 * 
 * @property \App\Models\MMenu $m_menu
 * @property \Illuminate\Database\Eloquent\Collection $m_menus
 * @property \Illuminate\Database\Eloquent\Collection $users_role_menus
 *
 * @package App\Models
 */
class MMenu extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'm_menus_id' => 'int'
	];

	protected $fillable = [
		'name',
		'url',
		'm_menus_id'
	];

	public function m_menu()
	{
		return $this->belongsTo(\App\Models\MMenu::class, 'm_menus_id');
	}

	public function m_menus()
	{
		return $this->hasMany(\App\Models\MMenu::class, 'm_menus_id');
	}

	public function users_role_menus()
	{
		return $this->hasMany(\App\Models\UsersRoleMenu::class, 'm_menus_id');
	}
}
