<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 01 Sep 2019 14:43:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class MPeriode
 * 
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon $date_start
 * @property \Carbon\Carbon $date_end
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $is_closed
 * 
 * @property \Illuminate\Database\Eloquent\Collection $periode_products
 *
 * @package App\Models
 */
class MPeriode extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'm_periode';

	protected $casts = [
		'is_closed' => 'int'
	];

	protected $dates = [
		'date_start',
		'date_end'
	];

	protected $fillable = [
		'name',
		'date_start',
		'date_end',
		'is_closed'
	];

	public function periode_products()
	{
		return $this->hasMany(\App\Models\PeriodeProduct::class);
	}
}
