<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 01 Sep 2019 14:43:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class MProduct
 *
 * @property int $id
 * @property string $code
 * @property string $name
 * @property int $m_product_category_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 *
 * @property \App\Models\MProductCategory $m_product_category
 * @property \App\Models\MUser $m_user
 * @property \Illuminate\Database\Eloquent\Collection $m_product_price_buys
 * @property \Illuminate\Database\Eloquent\Collection $periode_products
 * @property \Illuminate\Database\Eloquent\Collection $t_form_pengeluaran_barang_ds
 * @property \Illuminate\Database\Eloquent\Collection $t_form_permintaan_barang_ds
 * @property \Illuminate\Database\Eloquent\Collection $t_procurement_ds
 * @property \Illuminate\Database\Eloquent\Collection $t_purchase_order_ds
 *
 * @package App\Models
 */
class MProduct extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'm_product';

	protected $casts = [
		'm_product_category_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int'
	];

	protected $fillable = [
		'code',
		'name',
		'm_product_category_id',
        'last_stock',
		'created_by',
		'updated_by'
	];

	protected $appends = [
		'label',
		'price_buy',
		'key',
	];

	public function m_product_category()
	{
		return $this->belongsTo(\App\Models\MProductCategory::class);
	}

	public function m_user()
	{
		return $this->belongsTo(\App\Models\MUser::class, 'updated_by');
	}

	public function m_product_price_buys()
	{
		return $this->hasMany(\App\Models\MProductPriceBuy::class);
	}

	public function periode_products()
	{
		return $this->hasMany(\App\Models\PeriodeProduct::class);
	}

	public function t_form_pengeluaran_barang_ds()
	{
		return $this->hasMany(\App\Models\TFormPengeluaranBarangD::class);
	}

	public function t_form_permintaan_barang_ds()
	{
		return $this->hasMany(\App\Models\TFormPermintaanBarangD::class);
	}

	public function t_procurement_ds()
	{
		return $this->hasMany(\App\Models\TProcurementD::class);
	}

	public function t_purchase_order_ds()
	{
		return $this->hasMany(\App\Models\TPurchaseOrderD::class);
	}

	public function getLabelAttribute()
	{
		return '(' . $this->code . ') ' . $this->name;
	}

	public function getPriceBuyAttribute()
	{
		return ($this->m_product_price_buys()->latest()->first() ? $this->m_product_price_buys()->latest()->first()->price : 0);
	}

	public static function getTableName()
	{
		return with(new static)->getTable();
	}
    public function getKeyAttribute()
    {
        return $this->table;
    }
}
