<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 01 Sep 2019 14:43:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class MProductCategory
 * 
 * @property int $id
 * @property string $code
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * 
 * @property \App\Models\MUser $m_user
 * @property \Illuminate\Database\Eloquent\Collection $m_products
 *
 * @package App\Models
 */
class MProductCategory extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'm_product_category';

	protected $casts = [
		'created_by' => 'int',
		'updated_by' => 'int'
	];

	protected $fillable = [
		'code',
		'name',
		'created_by',
		'updated_by'
	];

	protected $appends = [
		'label',
		'key',
	];

	public static function getColumns()
	{
		return [
			[
				'headerName' => 'ID',
				'field' => 'id',
				'hide' => true,
			], [
				'headerName' => 'Code',
				'field' => 'code',
				'hide' => false,
			], [
				'headerName' => 'Name',
				'field' => 'name',
				'hide' => false,
			], [
				'headerName' => 'Create At',
				'field' => 'created_at',
				'hide' => false,
			], [
				'headerName' => 'Update At',
				'field' => 'updated_at',
				'hide' => false,
			],
		];
	}


	public function m_user()
	{
		return $this->belongsTo(\App\Models\MUser::class, 'updated_by');
	}

	public function m_products()
	{
		return $this->hasMany(\App\Models\MProduct::class);
	}
	
    public function getLabelAttribute()
    {
        return '('.$this->code.') '.$this->name;
    }
	
    public function getKeyAttribute()
    {
        return $this->table;
    }
}
