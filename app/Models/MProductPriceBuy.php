<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 01 Sep 2019 14:43:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class MProductPriceBuy
 * 
 * @property int $id
 * @property int $m_product_id
 * @property \Carbon\Carbon $date_start
 * @property \Carbon\Carbon $date_end
 * @property float $price
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $created_by
 * @property int $updated_by
 * 
 * @property \App\Models\MProduct $m_product
 * @property \App\Models\MUser $m_user
 *
 * @package App\Models
 */
class MProductPriceBuy extends Eloquent
{
	protected $table = 'm_product_price_buy';

	protected $casts = [
		'm_product_id' => 'int',
		'price' => 'float',
		'created_by' => 'int',
		'updated_by' => 'int'
	];

	protected $dates = [
		'date_start',
		'date_end'
	];

	protected $fillable = [
		'm_product_id',
		'date_start',
		'date_end',
		'price',
		'created_by',
		'updated_by'
	];

	public function m_product()
	{
		return $this->belongsTo(\App\Models\MProduct::class);
	}

	public function m_user()
	{
		return $this->belongsTo(\App\Models\MUser::class, 'updated_by');
	}
}
