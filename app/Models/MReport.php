<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 01 Sep 2019 14:43:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class MCustomer
 *
 * @property int $id
 * @property string $code
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @property \Illuminate\Database\Eloquent\Collection $t_work_orders
 *
 * @package App\Models
 */
class MReport extends Eloquent
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $table = 'm_report';

    protected $fillable = [
        'code',
        'name',
        'query'
    ];

    public function getLabelAttribute()
    {
        return '(' . $this->code . ') ' . $this->name;
    }

    public function getKeyAttribute()
    {
        return $this->table;
    }
}
