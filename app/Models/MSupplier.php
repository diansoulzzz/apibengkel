<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 01 Sep 2019 14:43:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class MSupplier
 * 
 * @property int $id
 * @property string $code
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $t_procurements
 * @property \Illuminate\Database\Eloquent\Collection $t_purchase_orders
 *
 * @package App\Models
 */
class MSupplier extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'm_supplier';

	protected $fillable = [
		'code',
		'name'
	];

	protected $appends = [
		'label',
		'key',
	];

	public function t_procurements()
	{
		return $this->hasMany(\App\Models\TProcurement::class);
	}

	public function t_purchase_orders()
	{
		return $this->hasMany(\App\Models\TPurchaseOrder::class);
	}

	public function getLabelAttribute()
	{
		return '(' . $this->code . ') ' . $this->name;
	}
    public function getKeyAttribute()
    {
        return $this->table;
    }
}
