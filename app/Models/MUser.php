<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 01 Sep 2019 14:43:36 +0000.
 */

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class MUser extends Authenticatable
{
    use Notifiable;

	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'm_users_role_id' => 'int'
	];

	protected $hidden = [
		'password',
		'api_token'
	];

	protected $fillable = [
		'name',
		'email',
		'password',
		'm_users_role_id',
		'api_token'
	];

	public function m_users_role()
	{
		return $this->belongsTo(\App\Models\MUsersRole::class);
	}

	public function t_form_pengeluaran_barangs()
	{
		return $this->hasMany(\App\Models\TFormPengeluaranBarang::class, 'updated_by');
	}

	public function m_products()
	{
		return $this->hasMany(\App\Models\MProduct::class, 'updated_by');
	}

	public function m_product_categories()
	{
		return $this->hasMany(\App\Models\MProductCategory::class, 'updated_by');
	}

	public function m_product_price_buys()
	{
		return $this->hasMany(\App\Models\MProductPriceBuy::class, 'updated_by');
	}

	public function t_form_pengeluaran_barang_photos()
	{
		return $this->hasMany(\App\Models\TFormPengeluaranBarangPhoto::class, 'updated_by');
	}

	public function t_form_permintaan_barangs()
	{
		return $this->hasMany(\App\Models\TFormPermintaanBarang::class, 'updated_by');
	}

	public function t_procurements()
	{
		return $this->hasMany(\App\Models\TProcurement::class, 'updated_by');
	}

	public function t_purchase_orders()
	{
		return $this->hasMany(\App\Models\TPurchaseOrder::class, 'updated_by');
	}

	public function t_work_orders()
	{
		return $this->hasMany(\App\Models\TWorkOrder::class, 'updated_by');
	}
}
