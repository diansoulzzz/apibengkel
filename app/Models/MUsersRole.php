<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 01 Sep 2019 14:43:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class MUsersRole
 * 
 * @property int $id
 * @property string $name
 * 
 * @property \Illuminate\Database\Eloquent\Collection $m_users
 * @property \Illuminate\Database\Eloquent\Collection $users_role_menus
 *
 * @package App\Models
 */
class MUsersRole extends Eloquent
{
	protected $table = 'm_users_role';
	public $timestamps = false;

	protected $fillable = [
		'name'
	];

	public function m_users()
	{
		return $this->hasMany(\App\Models\MUser::class);
	}

	public function users_role_menus()
	{
		return $this->hasMany(\App\Models\UsersRoleMenu::class);
	}
}
