<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 01 Sep 2019 14:43:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PeriodeProduct
 * 
 * @property int $id
 * @property int $m_periode_id
 * @property int $m_product_id
 * @property float $stock_start
 * @property float $stock_procurement
 * @property float $stock_used
 * @property float $stock_end
 * 
 * @property \App\Models\MPeriode $m_periode
 * @property \App\Models\MProduct $m_product
 *
 * @package App\Models
 */
class PeriodeProduct extends Eloquent
{
	protected $table = 'periode_product';
	public $timestamps = false;

	protected $casts = [
		'm_periode_id' => 'int',
		'm_product_id' => 'int',
		'stock_start' => 'float',
		'stock_procurement' => 'float',
		'stock_used' => 'float',
		'stock_end' => 'float'
	];

	protected $fillable = [
		'm_periode_id',
		'm_product_id',
		'stock_start',
		'stock_procurement',
		'stock_used',
		'stock_end'
	];

	public function m_periode()
	{
		return $this->belongsTo(\App\Models\MPeriode::class);
	}

	public function m_product()
	{
		return $this->belongsTo(\App\Models\MProduct::class);
	}
}
