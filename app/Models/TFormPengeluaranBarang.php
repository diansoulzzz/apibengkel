<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 01 Sep 2019 14:43:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TFormPengeluaranBarang
 * 
 * @property int $id
 * @property string $code
 * @property \Carbon\Carbon $trans_date
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $t_form_permintaan_barang_id
 * @property int $is_canceled
 * @property int $created_by
 * @property int $updated_by
 * 
 * @property \App\Models\MUser $m_user
 * @property \App\Models\TFormPermintaanBarang $t_form_permintaan_barang
 * @property \Illuminate\Database\Eloquent\Collection $t_form_pengeluaran_barang_ds
 * @property \Illuminate\Database\Eloquent\Collection $t_form_pengeluaran_barang_photos
 *
 * @package App\Models
 */
class TFormPengeluaranBarang extends Eloquent
{
	protected $table = 't_form_pengeluaran_barang';

	protected $casts = [
		't_form_permintaan_barang_id' => 'int',
		'is_canceled' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int'
	];

	protected $dates = [
		'trans_date'
	];

	protected $fillable = [
		'code',
		'trans_date',
		't_form_permintaan_barang_id',
		'is_canceled',
		'created_by',
		'updated_by'
	];

	public function m_user()
	{
		return $this->belongsTo(\App\Models\MUser::class, 'updated_by');
	}

	public function t_form_permintaan_barang()
	{
		return $this->belongsTo(\App\Models\TFormPermintaanBarang::class);
	}

	public function t_form_pengeluaran_barang_ds()
	{
		return $this->hasMany(\App\Models\TFormPengeluaranBarangD::class);
	}

	public function t_form_pengeluaran_barang_photos()
	{
		return $this->hasMany(\App\Models\TFormPengeluaranBarangPhoto::class);
	}
}
