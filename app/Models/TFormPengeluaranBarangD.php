<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 01 Sep 2019 14:43:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TFormPengeluaranBarangD
 * 
 * @property int $id
 * @property float $qty
 * @property int $m_product_id
 * @property int $f_form_pengeluaran_barang_id
 * @property int $t_form_permintaan_barang_d_id
 * 
 * @property \App\Models\TFormPengeluaranBarang $f_form_pengeluaran_barang
 * @property \App\Models\MProduct $m_product
 * @property \App\Models\TFormPermintaanBarangD $t_form_permintaan_barang_d
 *
 * @package App\Models
 */
class TFormPengeluaranBarangD extends Eloquent
{
	protected $table = 't_form_pengeluaran_barang_d';
	public $timestamps = false;

	protected $casts = [
		'qty' => 'float',
		'm_product_id' => 'int',
		'f_form_pengeluaran_barang_id' => 'int',
		't_form_permintaan_barang_d_id' => 'int'
	];

	protected $fillable = [
		'qty',
		'm_product_id',
		'f_form_pengeluaran_barang_id',
		't_form_permintaan_barang_d_id'
	];

	public function f_form_pengeluaran_barang()
	{
		return $this->belongsTo(\App\Models\TFormPengeluaranBarang::class);
	}

	public function m_product()
	{
		return $this->belongsTo(\App\Models\MProduct::class);
	}

	public function t_form_permintaan_barang_d()
	{
		return $this->belongsTo(\App\Models\TFormPermintaanBarangD::class);
	}
}
