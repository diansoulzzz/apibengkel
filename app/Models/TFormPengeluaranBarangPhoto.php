<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 01 Sep 2019 14:43:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TFormPengeluaranBarangPhoto
 * 
 * @property int $id
 * @property string $photo_url
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $f_form_pengeluaran_barang_id
 * @property int $created_by
 * @property int $updated_by
 * 
 * @property \App\Models\TFormPengeluaranBarang $f_form_pengeluaran_barang
 * @property \App\Models\MUser $m_user
 *
 * @package App\Models
 */
class TFormPengeluaranBarangPhoto extends Eloquent
{
	protected $table = 't_form_pengeluaran_barang_photo';

	protected $casts = [
		'f_form_pengeluaran_barang_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int'
	];

	protected $fillable = [
		'photo_url',
		'f_form_pengeluaran_barang_id',
		'created_by',
		'updated_by'
	];

	public function f_form_pengeluaran_barang()
	{
		return $this->belongsTo(\App\Models\TFormPengeluaranBarang::class);
	}

	public function m_user()
	{
		return $this->belongsTo(\App\Models\MUser::class, 'updated_by');
	}
}
