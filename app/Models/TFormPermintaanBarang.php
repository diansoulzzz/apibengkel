<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 01 Sep 2019 14:43:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TFormPermintaanBarang
 * 
 * @property int $id
 * @property string $code
 * @property \Carbon\Carbon $trans_date
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $t_work_order_id
 * @property int $is_canceled
 * @property int $created_by
 * @property int $updated_by
 * 
 * @property \App\Models\TWorkOrder $t_work_order
 * @property \App\Models\MUser $m_user
 * @property \Illuminate\Database\Eloquent\Collection $f_form_pengeluaran_barangs
 * @property \Illuminate\Database\Eloquent\Collection $t_form_permintaan_barang_ds
 *
 * @package App\Models
 */
class TFormPermintaanBarang extends Eloquent
{
	protected $table = 't_form_permintaan_barang';

	protected $casts = [
		't_work_order_id' => 'int',
		'is_canceled' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int'
	];

	protected $dates = [
		'trans_date'
	];

	protected $fillable = [
		'code',
		'trans_date',
		't_work_order_id',
		'is_canceled',
		'created_by',
		'updated_by'
	];

	protected $appends = [
		'label',
		'key'
	];

	public function t_work_order()
	{
		return $this->belongsTo(\App\Models\TWorkOrder::class);
	}

	public function m_user()
	{
		return $this->belongsTo(\App\Models\MUser::class, 'updated_by');
	}

	public function t_form_pengeluaran_barangs()
	{
		return $this->hasMany(\App\Models\TFormPengeluaranBarang::class);
	}

	public function t_form_permintaan_barang_ds()
	{
		return $this->hasMany(\App\Models\TFormPermintaanBarangD::class);
	}

	public function getLabelAttribute()
	{
		return '(' . $this->code . ') ' . $this->name;
	}
    public function getKeyAttribute()
    {
        return $this->table;
    }
}
