<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 01 Sep 2019 14:43:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TFormPermintaanBarangD
 * 
 * @property int $id
 * @property float $qty
 * @property int $m_product_id
 * @property int $t_form_permintaan_barang_id
 * 
 * @property \App\Models\MProduct $m_product
 * @property \App\Models\TFormPermintaanBarang $t_form_permintaan_barang
 * @property \Illuminate\Database\Eloquent\Collection $t_form_pengeluaran_barang_ds
 *
 * @package App\Models
 */
class TFormPermintaanBarangD extends Eloquent
{
	protected $table = 't_form_permintaan_barang_d';
	public $timestamps = false;

	protected $casts = [
		'qty' => 'float',
		'm_product_id' => 'int',
		't_form_permintaan_barang_id' => 'int'
	];

	protected $fillable = [
		'qty',
		'm_product_id',
		't_form_permintaan_barang_id'
	];

	protected $appends = [
		'code',
		'name',
	];

	public function m_product()
	{
		return $this->belongsTo(\App\Models\MProduct::class);
	}

	public function t_form_permintaan_barang()
	{
		return $this->belongsTo(\App\Models\TFormPermintaanBarang::class);
	}

	public function t_form_pengeluaran_barang_ds()
	{
		return $this->hasMany(\App\Models\TFormPengeluaranBarangD::class);
	}

	public function getCodeAttribute()
	{
		return $this->m_product->code;
	}

	public function getNameAttribute()
	{
		return $this->m_product->name;
	}
}
