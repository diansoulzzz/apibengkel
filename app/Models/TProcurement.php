<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 01 Dec 2019 06:26:57 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TProcurement
 * 
 * @property int $id
 * @property string $code
 * @property \Carbon\Carbon $trans_date
 * @property float $grand_total
 * @property string $notes
 * @property int $is_canceled
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $m_supplier_id
 * @property int $t_purchase_order_id
 * 
 * @property \App\Models\MSupplier $m_supplier
 * @property \App\Models\MUser $m_user
 * @property \App\Models\TPurchaseOrder $t_purchase_order
 * @property \Illuminate\Database\Eloquent\Collection $t_procurement_ds
 *
 * @package App\Models
 */
class TProcurement extends Eloquent
{
	protected $table = 't_procurement';

	protected $casts = [
		'grand_total' => 'float',
		'is_canceled' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'm_supplier_id' => 'int',
		't_purchase_order_id' => 'int'
	];

	protected $dates = [
		'trans_date'
	];

	protected $fillable = [
		'code',
		'trans_date',
		'grand_total',
		'notes',
		'is_canceled',
		'created_by',
		'updated_by',
		'm_supplier_id',
		't_purchase_order_id'
	];

	public function m_supplier()
	{
		return $this->belongsTo(\App\Models\MSupplier::class);
	}

	public function m_user()
	{
		return $this->belongsTo(\App\Models\MUser::class, 'updated_by');
	}

	public function t_purchase_order()
	{
		return $this->belongsTo(\App\Models\TPurchaseOrder::class);
	}

	public function t_procurement_ds()
	{
		return $this->hasMany(\App\Models\TProcurementD::class);
	}
}
