<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 01 Dec 2019 06:27:06 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TProcurementD
 *
 * @property int $id
 * @property int $t_procurement_id
 * @property int $m_product_id
 * @property float $qty
 * @property float $price
 * @property float $subtotal
 * @property int $t_purchase_order_d_id
 *
 * @property \App\Models\MProduct $m_product
 * @property \App\Models\TProcurement $t_procurement
 * @property \App\Models\TPurchaseOrderD $t_purchase_order_d
 *
 * @package App\Models
 */
class TProcurementD extends Eloquent
{
	protected $table = 't_procurement_d';
	public $timestamps = false;

	protected $casts = [
		't_procurement_id' => 'int',
		'm_product_id' => 'int',
		'qty' => 'float',
		'price' => 'float',
		'subtotal' => 'float',
		't_purchase_order_d_id' => 'int'
	];

	protected $fillable = [
		't_procurement_id',
		'm_product_id',
		'qty',
		'price',
		'subtotal',
		't_purchase_order_d_id'
	];

    protected $appends = [
        'code',
        'name'
    ];

	public function m_product()
	{
		return $this->belongsTo(\App\Models\MProduct::class);
	}

	public function t_procurement()
	{
		return $this->belongsTo(\App\Models\TProcurement::class);
	}

	public function t_purchase_order_d()
	{
		return $this->belongsTo(\App\Models\TPurchaseOrderD::class);
	}

    public function getCodeAttribute()
    {
        return $this->m_product->code;
    }

    public function getNameAttribute()
    {
        return $this->m_product->name;
    }
}
