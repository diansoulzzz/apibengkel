<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 01 Dec 2019 06:27:54 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TPurchaseOrder
 * 
 * @property int $id
 * @property string $code
 * @property \Carbon\Carbon $trans_date
 * @property float $grand_total
 * @property string $notes
 * @property int $is_closed
 * @property int $is_canceled
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $m_supplier_id
 * 
 * @property \App\Models\MSupplier $m_supplier
 * @property \App\Models\MUser $m_user
 * @property \Illuminate\Database\Eloquent\Collection $t_procurements
 * @property \Illuminate\Database\Eloquent\Collection $t_purchase_order_ds
 *
 * @package App\Models
 */
class TPurchaseOrder extends Eloquent
{
	protected $table = 't_purchase_order';

	protected $casts = [
		'grand_total' => 'float',
		'is_closed' => 'int',
		'is_canceled' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'm_supplier_id' => 'int'
	];

	protected $dates = [
		'trans_date'
	];

	protected $fillable = [
		'code',
		'trans_date',
		'grand_total',
		'notes',
		'is_closed',
		'is_canceled',
		'created_by',
		'updated_by',
		'm_supplier_id'
	];

	protected $appends = [
		'label',
		'key'
	];

	public function m_supplier()
	{
		return $this->belongsTo(\App\Models\MSupplier::class);
	}

	public function m_user()
	{
		return $this->belongsTo(\App\Models\MUser::class, 'updated_by');
	}

	public function t_procurements()
	{
		return $this->hasMany(\App\Models\TProcurement::class);
	}

	public function t_purchase_order_ds()
	{
		return $this->hasMany(\App\Models\TPurchaseOrderD::class);
	}

	public function getLabelAttribute()
	{
		return '(' . $this->code . ') ';
	}

    public function getKeyAttribute()
    {
        return $this->table;
    }
}
