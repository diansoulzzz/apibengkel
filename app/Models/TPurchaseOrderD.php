<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 01 Dec 2019 06:28:30 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TPurchaseOrderD
 *
 * @property int $id
 * @property int $t_purchase_order_id
 * @property int $m_product_id
 * @property float $qty
 * @property float $price
 * @property float $subtotal
 *
 * @property \App\Models\MProduct $m_product
 * @property \App\Models\TPurchaseOrder $t_purchase_order
 * @property \Illuminate\Database\Eloquent\Collection $t_procurement_ds
 *
 * @package App\Models
 */
class TPurchaseOrderD extends Eloquent
{
	protected $table = 't_purchase_order_d';
	public $timestamps = false;

	protected $casts = [
		't_purchase_order_id' => 'int',
		'm_product_id' => 'int',
		'qty' => 'float',
		'price' => 'float',
		'subtotal' => 'float'
	];

	protected $fillable = [
		't_purchase_order_id',
		'm_product_id',
		'qty',
		'price',
		'subtotal'
	];

	protected $appends = [
		'code',
		'name',
        't_purchase_order_d_id'
	];

	public function m_product()
	{
		return $this->belongsTo(\App\Models\MProduct::class);
	}

	public function t_purchase_order()
	{
		return $this->belongsTo(\App\Models\TPurchaseOrder::class);
	}

	public function t_procurement_ds()
	{
		return $this->hasMany(\App\Models\TProcurementD::class);
	}

	public function getCodeAttribute()
	{
		return $this->m_product->code;
	}

	public function getNameAttribute()
	{
		return $this->m_product->name;
	}

    public function getTPurchaseOrderDIdAttribute()
    {
        return $this->id;
    }
}
