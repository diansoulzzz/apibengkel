<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 01 Sep 2019 14:43:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TWorkOrder
 *
 * @property int $id
 * @property string $code
 * @property \Carbon\Carbon $trans_date
 * @property string $complaint
 * @property int $m_customer_id
 * @property float $estimate_day
 * @property float $estimate_hour
 * @property string $plate_number
 * @property string $chassis_number
 * @property string $hull_number
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $m_mekanik_id
 * @property int $status
 * @property int $is_canceled
 * @property int $created_by
 * @property int $updated_by
 *
 * @property \App\Models\MCustomer $m_customer
 * @property \App\Models\MMekanik $m_mekanik
 * @property \App\Models\MUser $m_user
 * @property \Illuminate\Database\Eloquent\Collection $t_form_permintaan_barangs
 *
 * @package App\Models
 */
class TWorkOrder extends Eloquent
{
	protected $table = 't_work_order';

	protected $casts = [
		'm_customer_id' => 'int',
		'estimate_day' => 'float',
		'estimate_hour' => 'float',
		'm_mekanik_id' => 'int',
		'status' => 'int',
		'is_canceled' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int'
	];

	protected $dates = [
		'trans_date'
	];

	protected $fillable = [
		'code',
		'trans_date',
		'complaint',
		'm_customer_id',
		'estimate_day',
		'estimate_hour',
		'plate_number',
		'chassis_number',
		'hull_number',
		'm_mekanik_id',
		'status',
		'is_canceled',
		'created_by',
		'updated_by'
	];

	protected $appends = [
		'label',
		'key'
	];

	public function m_customer()
	{
		return $this->belongsTo(\App\Models\MCustomer::class);
	}

	public function m_mekanik()
	{
		return $this->belongsTo(\App\Models\MMekanik::class);
	}

	public function m_user()
	{
		return $this->belongsTo(\App\Models\MUser::class, 'updated_by');
	}

	public function t_form_permintaan_barangs()
	{
		return $this->hasMany(\App\Models\TFormPermintaanBarang::class);
	}

    public function t_work_order_photos()
    {
        return $this->hasMany(\App\Models\TWorkOrderPhoto::class);
    }

	public function getLabelAttribute()
	{
		return '(' . $this->code . ') ' . $this->name;
	}
    public function getKeyAttribute()
    {
        return $this->table;
    }
}
