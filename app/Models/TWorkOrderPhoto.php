<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 02 Dec 2019 11:50:08 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;

/**
 * Class TWorkOrderPhoto
 *
 * @property int $id
 * @property string $photo_url
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $t_work_order_id
 *
 * @property \App\Models\MUser $m_user
 * @property \App\Models\TWorkOrder $t_work_order
 *
 * @package App\Models
 */
class TWorkOrderPhoto extends Eloquent
{
    protected $table = 't_work_order_photo';

    protected $casts = [
        'created_by' => 'int',
        'updated_by' => 'int',
        't_work_order_id' => 'int'
    ];

    protected $fillable = [
        'photo_url',
        'created_by',
        'updated_by',
        't_work_order_id'
    ];
    protected $appends = ['src'];

    public function m_user()
    {
        return $this->belongsTo(\App\Models\MUser::class, 'updated_by');
    }

    public function t_work_order()
    {
        return $this->belongsTo(\App\Models\TWorkOrder::class);
    }

    public function getPhotoUrlAttribute($value)
    {
        if (filter_var($value, FILTER_VALIDATE_URL)) {
            return $value;
        }
        return Storage::disk('public')->url($value);
    }

    public function getSrcAttribute($value)
    {
        return $this->photo_url;
    }
}
