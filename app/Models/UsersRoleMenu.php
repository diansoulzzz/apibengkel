<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 01 Sep 2019 14:43:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UsersRoleMenu
 * 
 * @property int $id
 * @property int $m_users_role_id
 * @property int $m_menus_id
 * 
 * @property \App\Models\MMenu $m_menu
 * @property \App\Models\MUsersRole $m_users_role
 *
 * @package App\Models
 */
class UsersRoleMenu extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'm_users_role_id' => 'int',
		'm_menus_id' => 'int'
	];

	protected $fillable = [
		'm_users_role_id',
		'm_menus_id'
	];

	public function m_menu()
	{
		return $this->belongsTo(\App\Models\MMenu::class, 'm_menus_id');
	}

	public function m_users_role()
	{
		return $this->belongsTo(\App\Models\MUsersRole::class);
	}
}
