<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link rel="stylesheet" href="{{asset('assets/vendor/bootstrap/bootstrap.min.css')}}"/>
<style>
    thead {
        display: table-header-group !important;
    }

    tfoot {
        display: table-row-group !important;
    }

    tr {
        page-break-inside: avoid !important;
    }

    @page {
        margin: 0cm 0cm;
    }
</style>
