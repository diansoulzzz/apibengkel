<!DOCTYPE html>
<html lang="en">

<head>
    @include('prints.include.css')
</head>
<body>
<div class="container">
    <div class="">
        <table class="table table-sm table-bordered">
            <thead>
            <tr>
                <th class="text-center">NO</th>
                <th class="text-center">NAMA BARANG</th>
                <th class="text-center">JUMLAH</th>
                <th class="text-center">HARGA SATUAN</th>
                <th class="text-center">SUBTOTAL</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($data->t_procurement_ds as $key => $detail)
                <tr>
                    <td class="text-center">{{$key+1}}</td>
                    <td class="left strong">{{$detail->m_product->name}}</td>
                    <td class="text-center">{{$detail->qty}}</td>
                    <td class="text-right">Rp. {{number_format($detail->price,0)}}</td>
                    <td class="text-right">Rp. {{number_format($detail->subtotal,0)}}</td>
                </tr>
            @endforeach
            <!-- @for ($i = 0; $i < 20; $i++)
            @endfor -->
            </tbody>
        </table>
    </div>
</div>
</body>

</html>
