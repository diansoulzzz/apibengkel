<!DOCTYPE html>
<html lang="en">

<head>
    @include('prints.include.css')
</head>
<body>
<div class="container">
    <div class="">
        <table class="table table-sm table-bordered">
            <thead>
            <tr>
                <th class="text-center">NO</th>
                <th class="text-center">NAMA BARANG</th>
                <th class="text-center">JUMLAH</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($data->t_form_permintaan_barang_ds as $key => $detail)
                <tr>
                    <td class="text-center">{{$key+1}}</td>
                    <td class="left strong">{{$detail->m_product->name}}</td>
                    <td class="text-center">{{$detail->qty}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
</body>

</html>
