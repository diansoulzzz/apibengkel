<!DOCTYPE html>
<html lang="en">

<head>
    @include('prints.include.css')
</head>

<body>
<div class="container">
    <div class="row">
        <div class="col">
            <p class="font-weight-bold">
                {{$data->m_customer->name}}
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <p class="text-right font-weight-light">
                NO : {{$data->code}}
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <p class="h4 text-center font-weight-bold">
                WORK ORDER (WO)
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col"></div>
        <div class="col">
            <p class="text-right">
                Tanggal : {{$data->trans_date->format('d F Y')}}
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <p class="text-left">
                TO : JAYA RAYA MOTOR
            </p>
        </div>
        <div class="col">
            <p class="text-right">
                Jam : {{$data->created_at->format('H:i')}}
            </p>
        </div>
    </div>
</div>
</body>

</html>
