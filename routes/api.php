<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('print')->group(function () {
    Route::get('purchase', 'Procurement\PurchaseController@printPurchase');
    Route::get('purchase-order', 'Procurement\PurchaseOrderController@printPurchaseOrder');
    Route::get('work-order', 'Project\WorkOrderController@printWorkOrder');
    Route::get('request-part', 'Project\RequestPartController@printRequestPart');
    Route::get('realization-part', 'Project\RealizationPartController@printRealizationPart');
});

Route::get('qwerty', 'Auth\AuthController@checkLicense');
Route::get('login', 'Auth\AuthController@unauthenticated')->name('login');
Route::post('login', 'Auth\AuthController@login');
Route::post('register', 'Auth\AuthController@register');
Route::get('stats', 'Auth\AuthController@checkStatus');

Route::middleware('auth:api')->group(function () {
    Route::prefix('customer')->group(function () {
        Route::get('data', 'Master\CustomerController@data');
        Route::get('list', 'Master\CustomerController@list');
        Route::post('entry', 'Master\CustomerController@entry');
        Route::get('newcode', 'Master\CustomerController@newCode');
    });

    Route::prefix('mekanik')->group(function () {
        Route::get('data', 'Master\MekanikController@data');
        Route::get('list', 'Master\MekanikController@list');
        Route::post('entry', 'Master\MekanikController@entry');
        Route::get('newcode', 'Master\MekanikController@newCode');
    });

    Route::prefix('supplier')->group(function () {
        Route::get('data', 'Master\SupplierController@data');
        Route::get('list', 'Master\SupplierController@list');
        Route::post('entry', 'Master\SupplierController@entry');
        Route::get('newcode', 'Master\SupplierController@newCode');
    });

    Route::prefix('product-category')->group(function () {
        Route::get('data', 'Master\ProductCategoryController@data');
        Route::get('list', 'Master\ProductCategoryController@list');
        Route::post('entry', 'Master\ProductCategoryController@entry');
        Route::get('newcode', 'Master\ProductCategoryController@newCode');
    });

    Route::prefix('product')->group(function () {
        Route::get('data', 'Master\ProductController@data');
        Route::get('list', 'Master\ProductController@list');
        Route::post('entry', 'Master\ProductController@entry');
        Route::get('newcode', 'Master\ProductController@newCode');
        Route::get('options', 'Master\ProductController@options');
    });

    Route::prefix('procurement')->group(function () {
        Route::prefix('purchase-order')->group(function () {
            Route::get('data', 'Procurement\PurchaseOrderController@data');
            Route::get('list', 'Procurement\PurchaseOrderController@list');
            Route::get('outstanding-list', 'Procurement\PurchaseOrderController@outstandingList');
            Route::post('entry', 'Procurement\PurchaseOrderController@entry');
            Route::get('options', 'Procurement\PurchaseOrderController@options');
            Route::prefix('search')->group(function () {
                Route::get('product', 'Procurement\PurchaseOrderController@searchProduct');
            });
        });
        Route::prefix('purchase')->group(function () {
            Route::get('data', 'Procurement\PurchaseController@data');
            Route::get('list', 'Procurement\PurchaseController@list');
            Route::post('entry', 'Procurement\PurchaseController@entry');
            Route::get('options', 'Procurement\PurchaseController@options');
            Route::prefix('search')->group(function () {
                Route::get('product', 'Procurement\PurchaseController@searchProduct');
            });
        });
        Route::prefix('receipt-goods')->group(function () {
            Route::get('data', 'Procurement\ReceiptGoodsController@data');
            Route::get('list', 'Procurement\ReceiptGoodsController@list');
            Route::post('entry', 'Procurement\ReceiptGoodsController@entry');
            Route::get('options', 'Procurement\ReceiptGoodsController@options');
            Route::prefix('search')->group(function () {
                Route::get('product', 'Procurement\ReceiptGoodsController@searchProduct');
            });
        });
    });
    Route::prefix('project')->group(function () {
        Route::prefix('work-order')->group(function () {
            Route::get('data', 'Project\WorkOrderController@data');
            Route::get('list', 'Project\WorkOrderController@list');
            Route::post('entry', 'Project\WorkOrderController@entry');
            Route::post('set-status', 'Project\WorkOrderController@setStatus');
            Route::get('options', 'Project\WorkOrderController@options');
            Route::prefix('search')->group(function () {
                Route::get('product', 'Project\WorkOrderController@searchProduct');
            });
            Route::get('upload/data', 'Project\WorkOrderController@dataUploadImage');
            Route::post('upload/entry', 'Project\WorkOrderController@entryUploadImage');
            Route::post('upload/temp/{woid}', 'Project\WorkOrderController@uploadImage');
        });
        Route::prefix('request-part')->group(function () {
            Route::get('data', 'Project\RequestPartController@data');
            Route::get('list', 'Project\RequestPartController@list');
            Route::post('entry', 'Project\RequestPartController@entry');
            Route::get('options', 'Project\RequestPartController@options');
            Route::prefix('search')->group(function () {
                Route::get('product', 'Project\RequestPartController@searchProduct');
            });
        });
        Route::prefix('realization-part')->group(function () {
            Route::get('data', 'Project\RealizationPartController@data');
            Route::get('list', 'Project\RealizationPartController@list');
            Route::post('entry', 'Project\RealizationPartController@entry');
            Route::get('options', 'Project\RealizationPartController@options');
            Route::prefix('search')->group(function () {
                Route::get('product', 'Project\RealizationPartController@searchProduct');
            });
        });
    });
    Route::prefix('report')->group(function () {
        Route::get('get-params', 'Report\ReportController@getParamFromID');
        Route::post('get-result', 'Report\ReportController@getResultFromID');
    });

    // Route::middleware('auth:api')->get('/user', function (Request $request) {
    //     return $request->user();
    // });

});
